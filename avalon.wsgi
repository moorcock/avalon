#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
 
# TODO: change to real paths
sys.path.insert(0, '/home/hosting_moorcock/projects/avalon/avalon')
sys.path.insert(0, '/home/hosting_moorcock/projects/avalon')
 
os.environ['DJANGO_SETTINGS_MODULE'] = 'avalon.development'
 
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
