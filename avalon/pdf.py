# -*- coding: utf-8 -*-

import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import RequestContext
from django.http import HttpResponse
from django.conf import settings
from cgi import escape

def render_to_pdf(request, template_src, context_dict):
    context_dict['MEDIA_ROOT'] = settings.MEDIA_ROOT
    template = get_template(template_src)
    context = RequestContext(request, context_dict)
    html  = template.render(context)
    result = StringIO.StringIO()

    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("utf-8")), result, encoding='UTF-8')
    if not pdf.err:
        response = HttpResponse(result.getvalue(), mimetype='application/pdf')
        # response['Content-Disposition'] = 'attachment; filename=Avalon_Favorites.pdf'
        return response
    return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))
