# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils import simplejson as json
from django.contrib.auth.decorators import login_required
from annoying.decorators import ajax_request
from avalon.realty.models import *
from avalon.realty.forms import SearchForm, Apperance, Order
from avalon.pdf import render_to_pdf

def search(request):
    form = SearchForm(request.GET)
    items = RealtyObject.objects.select_related(
        'country', 'city', 'type').filter(is_active=True)
    apperance = Apperance.LIST
    order = Order.PRICE
    
    # Filters, orders, apperance
    if form.is_valid():
        items = form.set_filters(items)
        if form.cleaned_data['apperance']:
            apperance = form.cleaned_data['apperance']
        if form.cleaned_data['order']:
            order = form.cleaned_data['order']
    #else:
    #    print form.errors # TODO: remove DEBUG
    
    # Pagination
    page = request.GET.get('page', 1)
    paginator = Paginator(items, settings.ITEMS_PER_PAGE)
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        page_obj = paginator.page(1)
    except EmptyPage:
        page_obj = paginator.page(paginator.num_pages)
    items_page = page_obj.object_list
    
    # Urls
    def get_url_for_param(param):
        params = request.GET.copy()
        if params.get(param): del params[param]
        as_url = params.urlencode()
        if len(as_url):
            return '?' + as_url + '&'
        else:
            return '?'
    url_paginator = get_url_for_param('page')
    url_apperance = get_url_for_param('apperance')
    url_order = get_url_for_param('order')
    
    # Json (for map)
    items_json = [];
    for item in items_page:
        image_url = getattr(item.cover, ('url_%sx%s' % settings.REALTY_COVER_IMAGE_SIZE))
        items_json.append(
            {
                'geolocation': item.geolocation,
                'title': item.title,
                'description': item.description,
                'price': item.price,
                'square': item.square,
                'cover': image_url,
                'url': item.url()
            }
        )
        
    # Favorites
    if request.user.is_authenticated():
        favor_ids = Favorite.objects.filter(
            user=request.user).values_list('object', flat=True)
        for item in items_page:
            if item.pk in favor_ids:
                item.in_favorites = True
    
    return render(
        request,
        'realty/search.html',
        {
            'page_obj': page_obj,
            'items': items_page,
            'items_count': paginator.count,
            'form': form,
            'apperance': apperance,
            'apperance_variants': Apperance.get_list(),
            'order': order,
            'order_variants': Order.get_list(),
            'url_paginator': url_paginator,
            'url_apperance': url_apperance,
            'url_order': url_order,
            'map_items_json': json.dumps(items_json)
        }
    )
    
def view(request, country, city, id):
    item = get_object_or_404(RealtyObject, pk=id)
    if country != item.country.title or city != item.city.title:
        return redirect(item.url())
    images = Image.objects.filter(object=item)
    item_json = {
        'geolocation': item.geolocation,
        'title': item.title
    }
    favor_ids = Favorite.objects.filter(
        user=request.user).values_list('object', flat=True)
    if item.pk in favor_ids:
        item.in_favorites = True
    return render(
        request,
        'realty/view.html',
        {
            'item': item,
            'images': images,
            'images_count': len(images),
            'map_items_json': json.dumps([item_json])
        }
    )

@login_required
def favorites(request):
    ids = Favorite.objects.filter(
        user=request.user).values_list('object', flat=True)
    items = RealtyObject.objects.filter(
        pk__in=ids).select_related('country', 'city', 'type')
    for item in items:
        item.in_favorites = True
    return render(request, 'realty/favorites.html',
        {'items': items, 'items_count': len(items)})
    
@login_required
def favorites_pdf(request):
    ids = Favorite.objects.filter(
        user=request.user).values_list('object', flat=True)
    items = RealtyObject.objects.filter(
        pk__in=ids).select_related('country', 'city', 'type')
    return render_to_pdf(request, 'realty/favorites_pdf.html',
        {'items': items, 'items_count': len(items)})

@login_required
@ajax_request
def favorites_add(request):
    if request.method == 'POST':
        id = request.POST.get('id', None)
        item = get_object_or_404(RealtyObject, pk=id)
        favor, created = Favorite.objects.get_or_create(
            object=item, user=request.user)
        return {'status': 'ok'}
    return {'status': 'error'}

@login_required
@ajax_request
def favorites_remove(request):
    if request.method == 'POST':
        id = request.POST.get('id', None)
        item = get_object_or_404(RealtyObject, pk=id)
        Favorite.objects.filter(
            object=item, user=request.user).delete()
        return {'status': 'ok'}
    return {'status': 'error'}
