from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(r'^realty/$', 'avalon.realty.views.search', name='search'),
    url(r'^(?P<country>\w+)/(?P<city>\w+)/(?P<id>\d+)/$', 'avalon.realty.views.view', name='view'),
    url(r'^favorites/$', 'avalon.realty.views.favorites', name='favorites'),
    url(r'^favorites/add$', 'avalon.realty.views.favorites_add', name='favorites_add'),
    url(r'^favorites/remove$', 'avalon.realty.views.favorites_remove', name='favorites_remove'),
    url(r'^favorites/pdf$', 'avalon.realty.views.favorites_pdf', name='favorites_pdf'),
)