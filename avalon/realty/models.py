# -*- coding: utf-8 -*-

import os
from django.db import models, transaction
from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete, pre_save
from django.conf import settings
from avalon.fields import ImageWithThumbsField
from avalon.realty.fields import GeoLocationField

class CustomParam(models.Model):
    title = models.CharField(max_length=200)
    
    class Meta:
        verbose_name = u'Параметр'
        verbose_name_plural = u'Параметры объектов'
        ordering = ["title"]

    def __unicode__(self):
        return self.title

class Country(models.Model):
    title = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    
    class Meta:
        verbose_name = u'Страна'
        verbose_name_plural = u'Страны'
        ordering = ["title"]

    def __unicode__(self):
        return self.title
    
class City(models.Model):
    country = models.ForeignKey(Country)
    title = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    
    class Meta:
        verbose_name = u'Город'
        verbose_name_plural = u'Города'
        ordering = ["country", "title"]

    def __unicode__(self):
        return self.title
    
class RealtyType(models.Model):
    title = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    
    class Meta:
        verbose_name = u'Тип объекта'
        verbose_name_plural = u'Типы объектов'
        ordering = ["title"]

    def __unicode__(self):
        return self.title
    
class Currency(models.Model):
    title = models.CharField(max_length=100)
    exchange_rate = models.FloatField()
    modified = models.DateTimeField(auto_now=True)
    
    class Meta:
        verbose_name = u'Валюта'
        verbose_name_plural = u'Валюты'
        ordering = ["id"]

    def __unicode__(self):
        return self.title
    
def get_thumb_upload_to(instance, filename):
        return os.path.join('realty', str(instance.object.id), filename)
    
class RealtyObject(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    price = models.FloatField(help_text='В Euro')
    square = models.FloatField()
    country = models.ForeignKey(Country)
    city = models.ForeignKey(City)
    type = models.ForeignKey(RealtyType)
    params = models.ManyToManyField(CustomParam, through='CustomParamValue')
    is_active = models.BooleanField()
    cover = ImageWithThumbsField(
        upload_to=get_thumb_upload_to,
        sizes=settings.REALTY_IMAGE_SIZES,
        editable=False,
        blank=True
    )
    count_images = models.IntegerField(editable=False, default=0, blank=True)
    address = models.CharField(max_length=200, default='', blank=True)
    geolocation = GeoLocationField(max_length=100, blank=True)
    
    class Meta:
        verbose_name = u'Объект недвижимости'
        verbose_name_plural = u'Объекты недвижимости'
        ordering = ["-created"]

    def __unicode__(self):
        return self.title
    
    def url(self):
        return "/%s/%s/%s" % (self.country.title, self.city.title, self.id)
    
class CustomParamValue(models.Model):
    object = models.ForeignKey(RealtyObject)
    param = models.ForeignKey(CustomParam)
    value = models.CharField(max_length=200)
    modified = models.DateTimeField(auto_now=True)
    
class ImageManager(models.Manager):                    
    # TODO: move to RealtyObjectManager?
    def update_realty_images_info(self, image):
        item = RealtyObject.objects.get(pk=image.object_id)
        item.count_images = Image.objects.filter(object=item).count()
        if image.is_cover:
            item.cover = image.image
        item.save()
    
class Image(models.Model):    
    object = models.ForeignKey(RealtyObject)
    image = ImageWithThumbsField(
        upload_to=get_thumb_upload_to,
        sizes=settings.REALTY_IMAGE_SIZES
    )
    is_cover = models.BooleanField()
    
    objects = ImageManager()
        
    class Meta:
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'
        ordering = ["-id"]

    def __unicode__(self):
        return "%s" % self.image

def presave_realty_image(sender, instance, raw, **kwargs):
    if not raw and not instance.is_cover:
        if instance.object.count_images == 0:
            instance.is_cover = True
            
def save_realty_image(sender, instance, created, **kwargs):
    Image.objects.update_realty_images_info(instance)
    # Should be only one cover photo
    if instance.is_cover:
        Image.objects.filter(
            object=instance.object, is_cover=True).exclude(
            pk=instance.pk).update(is_cover=False)

def delete_realty_image(sender, instance, **kwargs):
    Image.objects.update_realty_images_info(instance)

pre_save.connect(presave_realty_image, sender = Image)
post_save.connect(save_realty_image, sender = Image)
post_delete.connect(delete_realty_image, sender = Image)
        
class Favorite(models.Model):
    user = models.ForeignKey(User)
    object = models.ForeignKey(RealtyObject)
    created = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        verbose_name = u'Избранное'
        verbose_name_plural = u'Избранное'
        ordering = ["-created"]

    def __unicode__(self):
        return '%s - %s' % (self.user, self.object)
    