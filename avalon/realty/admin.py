# -*- coding: utf-8 -*-

from django.contrib import admin
from avalon.realty.models import *

class CountryAdmin(admin.ModelAdmin):
    list_display = ('title',)
    
class CityAdmin(admin.ModelAdmin):
    list_display = ('title',)

class RealtyTypeAdmin(admin.ModelAdmin):
    list_display = ('title',)

class RealtyObjectAdmin(admin.ModelAdmin):
    list_display = ('title',)

class ImageAdmin(admin.ModelAdmin):
    list_display = ('object', 'image', 'is_cover')
    
class CustomParamAdmin(admin.ModelAdmin):
    list_display = ('title',)

admin.site.register(Country, CountryAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(RealtyType, RealtyTypeAdmin)
admin.site.register(RealtyObject, RealtyObjectAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(CustomParam, CustomParamAdmin)
