# -*- coding: utf-8 -*-

from django import forms
from avalon.realty.models import Favorite, Country, City, RealtyType, Currency

class Apperance(object):
    LIST = 'list'
    TABLE = 'table'
    
    @classmethod
    def get_list(cls):
        return (('list', 'Список'), ('table', 'Таблица'))
    
class Order(object):
    PRICE = 'price'
    DATE = 'date'
    
    @classmethod
    def get_list(cls):
        return (('price', 'Стоимости'), ('date', 'Дате'))
        
class SearchForm(forms.Form):
    price_from = forms.DecimalField(min_value=1, max_value=100000000,
                                    label='Стоимость от', required=False)
    price_to = forms.DecimalField(min_value=1, max_value=100000000,
                                  label='до', required=False)
    currency = forms.TypedChoiceField(coerce=int, label='Валюта', required=False)
    square_from = forms.DecimalField(min_value=1, max_value=100000000,
                                     label='Площадь от', required=False)
    square_to = forms.DecimalField(min_value=1, max_value=100000000,
                                   label='до', required=False)
    country = forms.TypedMultipleChoiceField(coerce=int, widget=forms.CheckboxSelectMultiple,
                                             label='Страна', required=False)
    city = forms.TypedMultipleChoiceField(coerce=int, widget=forms.CheckboxSelectMultiple,
                                             label='Город', required=False)
    type = forms.TypedMultipleChoiceField(coerce=int, widget=forms.CheckboxSelectMultiple,
                                          label='Тип объекта', required=False)
    
    order = forms.ChoiceField(label='Сортировать по', required=False)
    apperance = forms.ChoiceField(label='Представление', required=False)
    
    def set_filters(self, query):
        country = self.cleaned_data['country']
        city = self.cleaned_data['city']
        type = self.cleaned_data['type']
        price_from = self.cleaned_data['price_from']
        price_to = self.cleaned_data['price_to']
        square_from = self.cleaned_data['square_from']
        square_to = self.cleaned_data['square_to']
        order = self.cleaned_data['order']
        currency = self.cleaned_data['currency']
        
        if country:
            query = query.filter(country__pk__in=country)
        if city:
            query = query.filter(city__pk__in=city)
        if type:
            query = query.filter(type__pk__in=type)
        
        # TODO: currency exchange rate
        if price_from:
            query = query.filter(price__gte=price_from)
        if price_to:
            query = query.filter(price__lte=price_to)
            
        if square_from:
            query = query.filter(square__gte=square_from)
        if square_to:
            query = query.filter(square__lte=square_to)
            
        if order:
            if order == Order.PRICE:
                query = query.order_by('price')
            else:
                query = query.order_by('-created')
        else:
            query = query.order_by('-created')
        
        return query
    
    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        
        self.fields['country'].choices = [(item.id, item.title)
            for item in Country.objects.all()]
        self.fields['city'].choices = [(item.id, item.title)
            for item in City.objects.all()]
        self.fields['type'].choices = [(item.id, item.title)
            for item in RealtyType.objects.all()]
        self.fields['currency'].choices = [(item.id, item.title)
            for item in Currency.objects.all()]
        
        self.fields['order'].choices = [(item[0], item[1])
            for item in Order.get_list()]
        self.fields['apperance'].choices = [(item[0], item[1])
            for item in Apperance.get_list()]
