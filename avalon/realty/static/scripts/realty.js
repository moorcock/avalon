
$( function() {
    $('#searchOrder').change(function () {
        var sel = $(this);
        window.location = sel.attr('data-url') + sel.val();
    });
    $('.favorite-link').click(function () {
        var link = $(this);
        if (link.hasClass('favorite-link-inporgress')) {
            return false;
        }
        link.addClass('favorite-link-inporgress');
        // TODO: prettify this code
        if (link.hasClass('favorite-link-add')) {
            // Add to favorites
            $.post('/favorites/add', {'id': link.attr('data-id')}, function (result) {
                if (result.status == 'ok') {
                    link.removeClass('favorite-link-inporgress');
                    link.html(link.attr('data-title-remove'));
                    link.removeClass('favorite-link-add');
                }
            });
        } else {
            // Remove from favorites
            $.post('/favorites/remove', {'id': link.attr('data-id')}, function (result) {
                if (result.status == 'ok') {
                    link.removeClass('favorite-link-inporgress');
                    link.html(link.attr('data-title-add'));
                    link.addClass('favorite-link-add');
                    if (link.attr("data-remove-object") == "True") {
                        link.closest(".realty-item").fadeOut();
                    }
                }
            });
        }
        return false;
    });
});

var Map = new function () {
    
    var _map = null;
    var _items = null;
    var _itemsAdded = false;
    
    var initSearchGmap = function (items, id) {
        var center = new google.maps.LatLng(0, 0); // TODO: center point
        var options = {
            zoom: 8,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            noResize: false
        };
        _map = new google.maps.Map(document.getElementById(id), options);
        _items = items;
    }
    
    var initViewGmap = function (item, id) {
        var coords = item.geolocation.split(',');
        var center = new google.maps.LatLng(coords[0], coords[1]);
        var options = {
            zoom: 14,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            noResize: true
        };
        var viewMap = new google.maps.Map(document.getElementById(id), options);
        addMarker(viewMap, item, center, false);
    }
    
    var openMap = function () {
        if (_itemsAdded) {
            //_map.fitBounds(_map.getBounds());
            return;
        }
        var bounds = new google.maps.LatLngBounds();
        if (_items) {
            for (var i=0; i < _items.length; i++) {
                var coords = _items[i].geolocation.split(',');
                var point = new google.maps.LatLng(coords[0], coords[1]);
                addMarker(_map, _items[i], point, true);
                bounds.extend(point);
            };
            _map.fitBounds(bounds);
            _map.setCenter(bounds.getCenter());
            // TODO: refresh map and fix the glitch
        }
        _itemsAdded = true;
    }
    
    var addMarker = function (map, item, point, withInfo) {
        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: point,
            title: item.title
        });
        if (withInfo) {
            var contentString = '<b>'+item.title+'</b><br>'
                +'<img src="'+item.cover+'"><br>'
                +'<span class="price">'+item.price+'</span><br>'
                +'Площадь: '+item.square+'<br>';
            //console.log(contentString);
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            google.maps.event.addListener(marker, 'mouseover', function () {
                infowindow.open(_map, this);
            });
            google.maps.event.addListener(marker, 'mouseout', function () {
                infowindow.close();
            });
            google.maps.event.addListener(marker, 'click', function () {
                window.location = item.url
            });
        }
    }
    
    this.init = function (items) {
        if ($('#topMapWrapper').length) {
            $('#topMapHint').click(function() {
                var mapWrap = $('#topMapWrapper');
                if (mapWrap.hasClass('top-map-wrapper-open')) {
                    $(this).html($(this).attr('data-title-closed'));
                    mapWrap.removeClass('top-map-wrapper-open');
                } else {
                    $(this).html($(this).attr('data-title-opened'));
                    mapWrap.addClass('top-map-wrapper-open');
                    openMap();
                }
            });
            initSearchGmap(items, 'topMap');
        }
        if ($('#viewMap').length) {
            if (items.length) {
                initViewGmap(items[0], 'viewMap');
            }
        }
    }
}
