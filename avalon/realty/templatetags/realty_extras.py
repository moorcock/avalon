# -*- coding: utf-8 -*-

from django import template
from django.conf import settings
from avalon.realty.models import Country, Favorite

register = template.Library()

@register.inclusion_tag('realty/countries_list.html')
def countries_list():
    return {
        'countries': Country.objects.all()[:settings.COUNTRIES_IN_FOOTER]
    }
    
@register.inclusion_tag('realty/favorites_count.html')
def favorites_count(user):
    return {
        'favorites_count': Favorite.objects.filter(
                        user=user).count()
    }
    
@register.inclusion_tag('realty/favorite_link.html')
def favorite_link(item, user, remove_object=False):
    return {
        'item': item,
        'user': user,
        'remove_object': remove_object
    }
    
@register.inclusion_tag('realty/image.html')
def image(item):
    width, height = settings.REALTY_COVER_IMAGE_SIZE
    return {
        'item': item,
        'width': width,
        'height': height,
        'url': getattr(item.cover, ('url_%sx%s' % (width, height)))
    }
