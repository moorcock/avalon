from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic.simple import direct_to_template
from django.conf.urls.static import static
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',    
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    
    url(r'', include('avalon.main.urls')),
    url(r'', include('avalon.accounts.urls')),
    url(r'', include('avalon.realty.urls')),
    url(r'', include('avalon.news.urls')),
    url(r'', include('avalon.contacts.urls')),
    url(r'', include('avalon.pages.urls')),
)
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = 'avalon.main.views.error404'
handler500 = 'avalon.main.views.error500'
