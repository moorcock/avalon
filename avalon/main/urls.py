from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'avalon.main.views.home', name='home'),
    url(r'^home/$', 'avalon.main.views.home', name='home'),
)