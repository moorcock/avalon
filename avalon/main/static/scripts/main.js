/**
 * Description.
 *
 * @author moorcock
 */

$.ajaxSetup({ 
     beforeSend: function(xhr, settings) {
         function getCookie(name) {
             var cookieValue = null;
             if (document.cookie && document.cookie != '') {
                 var cookies = document.cookie.split(';');
                 for (var i = 0; i < cookies.length; i++) {
                     var cookie = jQuery.trim(cookies[i]);
                     // Does this cookie string begin with the name we want?
                 if (cookie.substring(0, name.length + 1) == (name + '=')) {
                     cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                     break;
                 }
             }
         }
         return cookieValue;
         }
         if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
             // Only send the token to relative URLs i.e. locally.
             xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
         }
     } 
});

$( function() {
    $('.dropdown-toggle').dropdown();
    $('.editable').each(function () {
        var ed = new Editable($(this));
    });
});

var Editable = function (wrapper) {
    var text = wrapper.find('.editable-text');
    var form = wrapper.find('.editable-form');
    
    var updateAjax = function (data, callback) {
        $.post(form.find('form').attr('action'), data, function (result) {
            callback(result);
        });
    }
    
    text.click(function () {
        wrapper.addClass('editable-edit');
        return false;
    });
    
    form.find('.editable-cancel').click(function () {
        wrapper.removeClass('editable-edit');
        return false;
    });
    
    form.find('.editable-submit').click(function () {
        var butt = $(this);
        butt.button('loading');
        wrapper.find('.editable-error').hide();
        updateAjax(form.find('form').serializeArray(), function (result) {
            butt.button('reset');
            if (result.status == 'ok') {
                text.find('.editable-result').html(result.result);
            } else {
                wrapper.find('.editable-error').html(result.error).show();
            }
            wrapper.removeClass('editable-edit');
        });
        return false;
    });
}