from django.shortcuts import render, redirect

def home(request):
    return render(request, 'main/home.html')
    
def error404(request):
    return render(request, 'main/error404.html')
    
def error500(request):
    return render(request, 'main/error500.html')
