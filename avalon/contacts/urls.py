from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(r'^contacts/$', 'avalon.contacts.show', name='contacts'),
)