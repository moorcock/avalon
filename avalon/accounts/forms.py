# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from avalon.accounts.models import UserProfile

class UserProfileForm(forms.Form):
    avatar = forms.ImageField()

class UserNameForm(forms.Form):
    first_name = forms.CharField(max_length=60)
    last_name = forms.CharField(max_length=60)

class RegistrationForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(attrs={'maxlength':75}))
    password = forms.CharField(widget=forms.PasswordInput(render_value=False),
                               min_length=3, max_length=12)
    subscribe = forms.BooleanField(widget=forms.CheckboxInput(), required=False)

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.
        
        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(u"Этот электронный адрес уже зарегистрирован. Введите другой.")
        return self.cleaned_data['email']
        
class LoginForm(AuthenticationForm):
    remember_me = forms.BooleanField(widget=forms.CheckboxInput(), required=False, initial=True)
