# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import PasswordResetForm
from forms import RegistrationForm, LoginForm, UserNameForm, UserProfileForm
from avalon.accounts.models import RegistrationProfile
from django.contrib.sites.models import RequestSite, Site
from django.contrib.auth.decorators import login_required
from annoying.decorators import ajax_request

@login_required    
def profile(request):
    user = request.user
    if user.first_name or user.last_name:
        username = "%s %s" % (user.first_name, user.last_name)
    else:
        username = None
    return render(
        request,
        'accounts/profile.html',
        {
            'username': username,
            'avatar_form': UserProfileForm()
        }
    )
    
@login_required
@ajax_request
def edit_profile(request):
    form = UserNameForm(request.POST or None)
    if form.is_valid():
        user = request.user
        user.first_name = form.cleaned_data['first_name']
        user.last_name = form.cleaned_data['last_name']
        user.save()
        return {'status': 'ok', 'result': "%s %s" % (user.first_name, user.last_name)}
    return {'status': 'error', 'error': u'Заполните имя и фамилию'}
    
@login_required
def change_avatar(request):
    if request.method == 'POST':
        form = UserProfileForm(request.POST, request.FILES)
        profile = request.user.get_profile()
        if form.is_valid():
            profile.avatar = form.cleaned_data['avatar']
            profile.save()
        return redirect('/profile/')
    return redirect('/profile/')

def login(request):
    if request.user.is_authenticated():
        return redirect('/home/')
    if request.method == 'POST':
        if not request.POST.get('remember_me', None):
            request.session.set_expiry(0)
    return auth_views.login(request, template_name='accounts/login.html', authentication_form=LoginForm)
    
def logout(request):
    return auth_views.logout(request, '/home/')
    
def sign_up(request):
    form = RegistrationForm(request.POST or None)
    if form.is_valid():
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']
        subscribe = form.cleaned_data['subscribe']
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)
        ip = request.META['REMOTE_ADDR']
        new_user = RegistrationProfile.objects.create_inactive_user(email, password, site, ip)
        return render(request, 'accounts/sign_up_complete.html')
    return render(request, 'accounts/sign_up.html', {'form': form})
    
def activate(request, activation_key):
    activated = RegistrationProfile.objects.activate_user(activation_key)
    return render(request, 'accounts/activation.html', {'success': activated})
