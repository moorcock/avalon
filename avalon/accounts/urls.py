from django.conf.urls.defaults import patterns, include, url
from django.contrib.auth import views as auth_views

urlpatterns = patterns('',
    url(r'^login/$', 'avalon.accounts.views.login'),
    url(r'^logout/$', 'avalon.accounts.views.logout'),
    url(r'^sign_up/$', 'avalon.accounts.views.sign_up'),
    url(r'^activate/(?P<activation_key>\w+)/$', 'avalon.accounts.views.activate'),
    
    url(r'^password_reset/$', auth_views.password_reset,
        {
            'template_name': 'accounts/password_reset.html',
            'email_template_name': 'accounts/password_reset_email.html',
        },
        name='auth_password_reset'
    ),
    url(r'^password_reset_done/$',
        auth_views.password_reset_done,
        {
             'template_name': 'accounts/password_reset_done.html'
        },
        name='auth_password_reset_done'),
    url(r'^password_reset_confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
        auth_views.password_reset_confirm,
        {
            'template_name': 'accounts/password_reset_confirm.html'
        },
        name='auth_password_reset_confirm'),
    url(r'^password_reset_complete/$',
        auth_views.password_reset_complete,
        {
            'template_name': 'accounts/password_reset_complete.html'
        },
        name='auth_password_reset_complete'),
    url(r'^profile/$', 'avalon.accounts.views.profile'),
    url(r'^profile/edit/$', 'avalon.accounts.views.edit_profile'),
    url(r'^profile/changeavatar/$', 'avalon.accounts.views.change_avatar'),
)
