# -*- coding: utf-8 -*-

import datetime
import random
import re
import os
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models, transaction
from django.template.loader import render_to_string
from django.utils.hashcompat import sha_constructor
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from avalon.fields import ImageWithThumbsField

class UserProfile(models.Model):
    user = models.OneToOneField(User)

    def _get_avatar_upload_to(instance, filename):
        split = filename.rsplit('.',1)
        return os.path.join('users', "%s.%s" % (str(instance.user.id), split[1]))
    
    avatar = ImageWithThumbsField(
        upload_to=_get_avatar_upload_to,
        sizes=((32,32),)
    )

def create_profile(sender, **kw):
    user = kw["instance"]
    if kw["created"]:
        profile = UserProfile(user=user)
        profile.save()

post_save.connect(create_profile, sender=User, dispatch_uid="users-profilecreation-signal")

SHA1_RE = re.compile('^[a-f0-9]{40}$')

class RegistrationManager(models.Manager):
    def activate_user(self, activation_key):
        # Make sure the key we're trying conforms to the pattern of a
        # SHA1 hash; if it doesn't, no point trying to look it up in
        # the database.
        if SHA1_RE.search(activation_key):
            try:
                profile = self.get(activation_key=activation_key)
            except self.model.DoesNotExist:
                return False
            if not profile.activation_key_expired():
                user = profile.user
                user.is_active = True
                user.save()
                profile.activated = True
                profile.save()
                return user
        return False
    
    @transaction.commit_on_success
    def create_inactive_user(self, email, password,
                             site, ip, send_email=True):
        new_user = User.objects.create_user(email, email, password)
        new_user.is_active = False
        new_user.save()

        registration_profile = self.create_profile(new_user, ip)

        if send_email:
            registration_profile.send_activation_email(site)

        return new_user

    def create_profile(self, user, ip_address):
        salt = sha_constructor(str(random.random())).hexdigest()[:5]
        activation_key = sha_constructor(salt+user.username).hexdigest()
        return self.create(user=user,
                           activation_key=activation_key, ip=ip_address)

class RegistrationProfile(models.Model):    
    user = models.ForeignKey(User, unique=True, verbose_name='user')
    activation_key = models.CharField(verbose_name='activation key', max_length=40)
    ip = models.IPAddressField()
    activated = models.BooleanField()
    
    objects = RegistrationManager()
    
    class Meta:
        verbose_name = "registration"
        verbose_name_plural = "registrations"
    
    def __unicode__(self):
        return u"Регистрация для %s" % self.user
    
    def activation_key_expired(self):
        expiration_date = datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS)
        return self.activated or \
               (self.user.date_joined + expiration_date <= datetime.datetime.now())

    def send_activation_email(self, site):
        ctx_dict = { 'activation_key': self.activation_key,
                     'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
                     'site': site }
        subject = render_to_string('accounts/activation_email_subject.txt',
                                   ctx_dict)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        
        message = render_to_string('accounts/activation_email.txt',
                                   ctx_dict)
        
        self.user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)
