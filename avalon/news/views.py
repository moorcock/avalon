# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from annoying.decorators import ajax_request
from avalon.contacts.models import *

def show(request):
    pass