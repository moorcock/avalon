from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(r'^news/$', 'avalon.news.list', name='news'),
)